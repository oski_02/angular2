# Tutorial: Tour of heroes
The grand plan for this tutorial is to build an app that helps a staffing agency manage its stable of heroes. You'll build a basic app that has many of the features you'd expect to find in a full-blown, data-driven app: acquiring and displaying a list of heroes, editing a selected hero's detail, and navigating among different views of heroic data.
- You'll use built-in directives to show and hide elements and display lists of hero data.
- You'll create components to display hero details and show an array of heroes.
- You'll use one-way data binding for read-only data.
- You'll add editable fields to update a model with two-way data binding.
- You'll bind component methods to user events, like keystrokes and clicks.
- You'll enable users to select a hero from a master list and edit that hero in the details view.
- You'll format data with pipes.
- You'll create a shared service to assemble the heroes.
- You'll use routing to navigate among different views and their components.

We will buid [this](https://angular.io/generated/live-examples/toh-pt6/eplnkr.html)

## The Hero Editor
Follow the [setup](https://angular.io/guide/setup) instructions for creating a new project named ```angular-tour-of-heroes```.
Steps for this part are [here](https://angular.io/tutorial/toh-pt1)

## Master/Detail
Steps for this part are [here](https://angular.io/tutorial/toh-pt2)

## Multiple Components
The ```AppComponent``` is doing *everything* at the moment. In the beginning, it showed details of a single hero. Then it became a master/detail form with both a list of heroes and the hero detail. Soon there will be new requirements and capabilities. You can't keep piling features on top of features in one component; that's not maintainable.

You'll need to break it up into sub-components, each focused on a specific task or workflow. Eventually, the ```AppComponent``` could become a simple shell that hosts those sub-components.

Steps for this part are [here](https://angular.io/tutorial/toh-pt3)

To **define a component**, you always import the ```Component``` symbol.
The ```@Component``` decorator provides the Angular metadata for the component. The CSS selector name, ```hero-detail```, will match the element tag that identifies this component within a parent component's template.

Always ```export``` the component class because you'll always ```import``` it elsewhere.

## Routing
Use the Angular router to enable navigation. [Routing and Navigation](https://angular.io/guide/router)

The Angular router is an external, optional Angular NgModule called ```RouterModule```. The router is a combination of multiple provided services (```RouterModule```), multiple directives (```RouterOutlet```, ```RouterLink```, ```RouterLinkActive```), and a configuration (```Routes```). You'll configure the routes first.

First be sure *<base href='...'>* exists, more info at [Set the base href](https://angular.io/guide/router)

#### Configuring routes
Create a configuration file for the app routes.

Routes tell the router which views to display when a user clicks a link or pastes a URL into the browser address bar.
#### Router Outlet
If you paste the path, /heroes, into the browser address bar at the end of the URL, the router should match it to the heroes route and display the HeroesComponent. However, you have to tell the router where to display the component. To do this, you can add a <router-outlet> element at the end of the template. RouterOutlet is one of the directives provided by the RouterModule. The router displays each component immediately below the <router-outlet> as users navigate through the app.
#### Router Links
Users shouldn't have to paste a route URL into the address bar. Instead, add an anchor tag to the template that, when clicked, triggers navigation to the HeroesComponent.
Note the routerLink binding in the anchor tag. The RouterLink directive (another of the RouterModule directives) is bound to a string that tells the router where to navigate when the user clicks the link.

## Routing Module
Most applications have many more routes and they add guard services to protect against unwanted or unauthorized navigations. (Read more about guard services in the [Route Guards](https://angular.io/guide/router#guards) section of the [Routing & Navigation](https://angular.io/guide/router) page.


# NOTES
## NgModel
Although ```NgModel``` is a valid Angular directive, it isn't available by default. It belongs to the optional ```FormsModule```. You must opt-in to using that module.
### Import the FormsModule
Open the ```app.module.ts``` file and import the ```FormsModule``` symbol from the ```@angular/forms``` library. Then add the ```FormsModule``` to the ```@NgModule``` metadata's ```imports``` array, which contains the list of external modules that the app uses.

Read more about [```FormsModule```](https://angular.io/api/forms/FormsModule) and ```ngModel``` in the [Two-way data binding with ngModel](https://angular.io/guide/forms#ngModel) section of the [Forms](https://angular.io/guide/forms) guide and the [Two-way binding with NgModel](https://angular.io/guide/template-syntax#ngModel) section of the [Template Syntax](https://angular.io/guide/template-syntax) guide.

## *ngFor
The (```*```) prefix to ```ngFor``` is a critical part of this syntax. It indicates that the ```<li>``` element and its children constitute a **master template**.

The ```ngFor``` directive iterates over the component's ```heroes``` array and renders an instance of this template for each hero in that array.

The ```let hero``` part of the expression identifies ```hero``` as the template input variable, which holds the current hero item for each iteration. You can reference this variable within the template to access the current hero's properties.

Read more about ```ngFor``` and template input variables in the [Showing an array property with *ngFor](https://angular.io/guide/displaying-data#ngFor) section of the [Displaying Data](https://angular.io/guide/displaying-data) page and the [ngFor](https://angular.io/guide/template-syntax#ngFor) section of the [Template Syntax](https://angular.io/guide/template-syntax) page.


## Compoments
The file and component names follow the standard described in the Angular [style guide](https://angular.io/guide/styleguide#naming).

- The component *class* name should be written in *upper camel case* and end in the word "Component". The hero detail component class is ```HeroDetailComponent```.

- The component ```file``` name should be spelled in [lower dash case](https://angular.io/guide/glossary#dash-case), each word separated by dashes, and end in ```.component.ts```. The ```HeroDetailComponent``` class goes in the ```hero-detail.component.ts``` file.


Every component must be declared in one—and only one—NgModule.
In general, the ```declarations``` array contains a list of application components, pipes, and directives that belong to the module. A component must be declared in a module before other components can reference it.

## Services

The naming convention for service files is the service name in lowercase followed by ```.service```. For a multi-word service name, use lower [dash-case](https://angular.io/guide/glossary). For example, the filename for ```SpecialSuperHeroService``` is ```special-super-hero.service.ts```.

The ```@Injectable()``` decorator tells TypeScript to emit metadata about the service. The metadata specifies that Angular may need to inject other dependencies into this service.

Although the ```HeroService``` doesn't have any dependencies at the moment, applying the ```@Injectable()``` decorator ​from the start ensures consistency and future-proofing.
nject the HeroService

#### Instead of using the new line, you'll add two lines.

* Add a constructor that also defines a private property.
* Add to the component's ```providers``` metadata.

The constructor itself does nothing. The parameter simultaneously defines a private ```heroService``` property and identifies it as a ```HeroService``` injection site.

To teach the injector how to make a ```HeroService```, add the following ```providers``` array property to the bottom of the component metadata in the ```@Component``` call.
The ```providers``` array tells Angular to create a fresh instance of the ```HeroService``` when it creates an ```AppComponent```. The ```AppComponent```, as well as its child components, can use that service to get hero data.


#### Async services and Promises
Eventually, the hero data will come from a remote server. When using a remote server, users don't have to wait for the server to respond; additionally, you aren't able to block the UI during the wait.

To coordinate the view with the response, you can use *Promises*, which is an asynchronous technique that changes the signature of the ```getHeroes()``` method.

A Promise essentially promises to call back when the results are ready. You ask an asynchronous service to do some work and give it a callback function. The service does that work and eventually calls the function with the results or an error.
