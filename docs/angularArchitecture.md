# Architecture Overview
The framework consist of several libreries, some of them core and some optional.
You write Angular applications by composing HTML *templates* with Angularized markup, writing *component* classes to manage those templates, adding application logic in *services*, and boxing components and services in *modules*.
Then you launch the app by *bootstrapping* the *root module*.

## Modules
Angular apps are modular and Angular has its own modularity system called [*NgModules*](https://angular.io/guide/ngmodule).
Every Angular app has at least one NgModule class, [the *root module*](https://angular.io/guide/bootstrapping), conventionally named ```AppModule```.
While the *root module* may be the only module in a small application, most apps have many more *feature modules*.
An NgModule, whether a *root* or *feature*, is a class with an ```@NgModule``` decorator.
> [**Decorators**](https://medium.com/google-developers/exploring-es7-decorators-76ecb65fb841#.x5c2ndtx0) are functions that modify JavaScript classes. Angular has many decorators that attach metadata to classes so that it knows what those classes mean and how they should work.

```NgModule``` is a decorator function that takes a single metadata object whose properties describe the module. The most important properties are:

* ```declarations``` - the *view classes* that belong to this module. Angular has three kinds of view classes: [components](https://angular.io/guide/architecture#components), [directives](https://angular.io/guide/architecture#directives), and [pipes](https://angular.io/guide/pipes).
* ```exports``` - the subset of declarations that should be visible and usable in the component [templates](https://angular.io/guide/architecture#templates) of other modules.
* ```imports``` - other modules whose exported classes are needed by component templates declared in *this* module.
* ```providers``` - creators of [services](https://angular.io/guide/architecture#services) that this module contributes to the global collection of services; they become accessible in all parts of the app.
* ```bootstrap``` - the main application view, called the *root component*, that hosts all other app views. Only the *root module* should set this ```bootstrap``` property.


Launch an application by *bootstrapping* its root module. During development you're likely to bootstrap the ```AppModule``` in a ```main.ts``` file.

### Angular libraries.
Angular ships as a collection of JavaScript modules. You can think of them as library modules.

Each Angular library name begins with the ```@angular``` prefix.

You install them with the npm package manager and import parts of them with JavaScript ```import``` statements.

## Components
A *component* controls a patch of screen called a *view*. You decide a component's application logic inside a class. The class interacts with the view through an AAngular ships as a collection of JavaScript modules. You can think of them as library modules.

Each Angular library name begins with the @angular prefix.

You install them with the npm package manager and import parts of them with JavaScript import statements.PI of properties and methods.

Your app can take action at each moment in this lifecycle through optional [lifecycle hooks](https://angular.io/guide/lifecycle-hooks), like ```ngOnInit()```.

## Templates
You define a component's view with its companion template. A template is a form of HTML that tells Angular how to render de component. A template looks like regular HTML but it does not. It uses Angular's [template syntax](https://angular.io/guide/template-syntax) 

## Metadata
Metadata tells Angular how to process a class.
The ```@Component``` decorator takes a required configuration object with the information Angular needs to create and present the component and its view.

Here are a few of the most useful ```@Component``` configuration options:

* ```selector```: CSS selector that tells Angular to create and insert an instance of this component where it finds a ```<tag>``` in parent HTML. 
* ```templateUrl```: module-relative address of this component's HTML template.
* ```providers```: array of dependency injection providers for services that the component requires. This is one way to tell Angular that the component's constructor requires a service.

The metadata in the ```@Component``` tells Angular where to get the major building blocks you specify for the component.

The template, metadata, and component together describe a view.


#### The architectural takeaway is that you must add metadata to your code so that Angular knows what to do. 

## Data binding.
Angular supports data binding, a mechanism for coordinating parts of a template with parts of a component. Add binding markup to the template HTML to tell Angular how to connect both sides. There are four forms of data binding syntax. 

| -- | DOM | relation | COMPONENT |
| --- | :---: | :---: | :---: |
| {{value}} |  | <---- |  |
| [property] = 'value' |  | <---- |  |
| (event) = 'handler' |  | ----> |  |
| [(ng-model)] = 'property |  | <----> |  |


Angular processes *all* data bindings once per JavaScript event cycle, from the root of the application component tree through all child components.

## Directives
Angular templates are *dynamic*. When Angular renders them, it transfomrs the DOM according to the instructions given by directives.

**A directive is a class with a ```@Directive``` decorator**.

**A component is a directive-with-a-template**

**A ```@Component``` decorator is actually a ```@Directive``` decorator extended with template-oriented features.**

Two other kinds of directives exists: *structural* and *attribute* directives

## Services
*Service* is a broad category encompassing any value, funciton, or feature that your application needs. A service is typically a class with a narrow, well-defined purpouse. It should do something specific and do it well.

Component classes should be lean. They don't fetch data from the server, validate user input, or log directly to the console. They delegate such tasks to services.

A component's job is to enable the user experience and nothing more. It mediates between the view and the application logic.

## Dependency injection.
*Dependecy injection* is a way to supply a new instance of a class with the fully-formed dependencies it requires. Most dependencies are services. Angular uses dependency injection to provide new components with the services they need.

Angular can tell which services a component needs by looking at the types of its constructor parameters. When Angular creates a component, it first asks an injector for the services that the component requires.

#### What is it?
An injector maintains a container of service instances that it has previously created. If a requested service instance is not in the container, the injector makes one and adds it to the container before returning the service to Angular. When all requested services have been resolved and returned, Angular can call the component's constructor with those services as arguments. This is *dependency injection*.

If the injector doesn't have a service, how does it know how to make one?

In brief, you must have previously registered a provider of the service with the injector. A provider is something that can create or return a service, typically the service class itself.

You can register providers in modules or in components.

In general, add providers to the [root module](https://angular.io/guide/architecture#modules) so that the same instance of a service is available everywhere.

Registering at a component level means you get a new instance of the service with each new instance of that component.

Points to remember about dependency injection:

* Dependency injection is wired into the Angular framework and used everywhere.

* The injector is the main mechanism.

	* An injector maintains a container of service instances that it created.
	* An injector can create a new service instance from a provider.

* A provider is a recipe for creating a service.

* Register providers with injectors.













