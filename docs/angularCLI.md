# Angular CLI guidelines
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

- **ng new <appname>**
Generate a new project and skeleton application. (takes some time)
Generate an "appname" folder:
appname/:
src/
e2e/
tsconfig.json
tslint.json
karma.conf.js
package.json
protractor.conf.js
README.md -> contains some basic information on how to use CLI commands.

- **ng serve --open**
*ng serve* launches the server, watches your files, and rebuilds the app as you make changes to those files.
*--open* will automatically open your browser on http://localhost:4200/


CLI creates the first Angular component. This is the *root* component and it is named *app-root*. it is at *./src/app/app.component.ts:
	
	import { Component } from '@angular/core';

	@Component({
  	selector: 'app-root',
  	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.css']
	})
	export class AppComponent {
  	title = 'app';
	}


## src/ folder
Your app lives in the src folder. All Angular components, templates, styles, images, and anything else your app needs go here. Any files outside of this folder are meant to support building your app.

| File | Purpose |
| --- | --- |
| app/app.component. {ts, html, css, spec.ts} | Defines the ```AppComponent``` along with an HTML template, CSS stylesheetm, and a unit test. It is the root component of waht will become a tree of nested components as the application evolves. |
| app/app.module.ts | Defines ```AppModule```, the [root module](https://angular.io/guide/bootstrapping) that tells Angular how to assemble the application. Right now it declares only the ```AppComponent``` |
| assets/* | A folder where you can put images and anything else to be copied wholesale when you build your application |
| environments/* | This folder contains one file for each of your destination environments, each exporting simple configuration variables to use in your application. The files are replaced on-the-fly when you build your app. You might use a diferent API endpoint for development than you do for production or maybe different analytics tokens. You might even use some mock services. Either way, the CLI has you covered. |
| favicon.ico | Every site wants to look good on the bookmark bar. Get started with your very own Angular icon |
| index.html |  The main HTML page that is served when someone visits your site. Most of the time you'll never need to edit it. The CLI automatically adds all ```js``` and ```css``` when building your app so you never nees to add any ```<script>``` or ```<link>``` tags here manually. |
| main.ts | The main entry point for your app. Compiles the application with the [JIT compiller](https://angular.io/guide/glossary#jit) and bootstraps the appication's root module (```AppModule```) to run in the browser. You can also use the [AOT compiler](https://angular.io/guide/glossary#ahead-of-time-aot-compilation) without changing any code by passing in ```--aot``` to ```ng build``` or ```ng serve``` |
| polyfills.ts | different browsers have different levels of support of the web standarts. Polyfills help normalize those differences. You should be pretty safe with ```core-js``` and ```zone.js```, but be sure to check out the [Browser Support guide](https://angular.io/guide/browser-support) for more information |
| styles.css | Your global styles go here. Most of the time you'll want to have local styles in your components for easier maintenance, but styles that affect all your app need to be in a central place |
| test.ts | This is the main entry point for your unit tests. It has some custom configuration that might be unfamiliar, but it's not something you'll need to edit. |
| tsconfig. {app,spec}.json | TypeScript compiler configuration for the Angular app (```tsconfig.app.json```) and for the unit tests (```tsconfig.spec.json```) |

## root folder
The src/ folder is just one of the items inside the project's root folder. Other files help you build, test, maintain, document, and deploy the app. These files go in the root folder next to ```src/```.

my-app:

	e2e

		app.e2e-spec.ts
		app.po.ts
		tsconfig.e2e.json

	node_modules/...
	src/...
	.angular-cli.json
	.editorconfig
	.gitignore
	karma.conf.js
	package.json
	protractor.conf.js
	README.js
	tsconfig.json
	tslint.json

| File | Purpose |
| --- | --- |
| `e2e/` | Inside ```e2e/``` live the end-to-end tests. They shouldn't be inside ```src/``` because e2e tests are really a separate app that just so happens to test your main app. That's also why they have their own ```tsconfig.e2e.json``` |
| `node_modules` | ```Node.js``` creates thes folder and puts all third party modules lised in ```package.json``` inside of it. |
| `.angular-cli.json` | Configuration for Angular CLI. In this file you can set several defaults and also configure what files are included when your project is build. Check out the official documentation if you want to know more. |
| `.editorconfig` | Simple configuration for your editor to make sure everyone that uses your project has the same basic configuration. Most editors support an ```.editorconfig``` file. See [http://editorconfig.org](http://editorconfig.org) for more information. |
| `.gitignore` | Git configuration to make sure autogenerated files are not commited to source control.|
| `karma.conf.js` | Unit test configuration for the [Karma test runner](https://karma-runner.github.io/), used when running ```ng test```. |
| `package.json` | ```npm``` configuration listing the third party packages your project uses. You can also add your own [custom scripts](https://docs.npmjs.com/misc/scripts) here. |
| `protractor.conf.js` | End-to-end test configuration for [Protractor](http://www.protractortest.org/), used when running ```ng e2e```. |
| `README.md` | Basic documentation for your project, pre-filled with CLI command information. Make sure to enhance it with project documentation so that anyone checking out the repo can build your app! |
| `tsconfig.json` | TypeScript compiler configuration for your IDE to pick up and give you helpful tooling. |
| `tslint.json` | Linting configuration for [TSLint](https://palantir.github.io/tslint/) together with [Codelyzer](http://codelyzer.com/), used when running ```ng lint```. Linting helps keep your code style consistent. |
